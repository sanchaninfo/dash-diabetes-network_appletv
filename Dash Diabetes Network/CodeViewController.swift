
/***
 **Module Name:  Code View Controller
 **File Name :   Code View Controller.swift
 **Project :   dev.damedashstudios.com
 **Copyright(c) : Dam Dash Studious.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : Activation Code details for pairing the device.
 */
import UIKit
import KeychainAccess

class CodeViewController: UIViewController {
    
    var code,deviceId,uuid,userId,status:String!
    var datadict = NSDictionary()
    var timer = Timer()
    var isAction = Bool()
    var userIdentifier = "userId"
    var existingUserId:String!
    
    @IBOutlet weak var CodeLabel: UILabel!
    @IBOutlet weak var activationCode: UILabel!
    @IBOutlet weak var codeview: UIView!
    @IBOutlet weak var clickbtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        let str = "To start watching instantly\n\n1. Register on a desktop or mobile device at:\n\(kBaseUrl)\n\n2. Go to MY ACCOUNT >> Device Pairing.\n Click ADD.\n\n3. Enter Activation Code to the right..."
        let attributedString = NSMutableAttributedString.init(string: str)
        
        let str1 = "To start watching instantly\n\n1. Register on a desktop or mobile device at:\n"
        let str2 = "To start watching instantly\n\n1. Register on a desktop or mobile device at:\n\(kBaseUrl)\n\n2. Go to MY ACCOUNT >> Device Pairing.\n "
        attributedString.addAttribute(NSFontAttributeName, value:self.CodeLabel.font, range:NSRange(location: 0, length: str.characters.count))
        
        
        attributedString.addAttribute(NSForegroundColorAttributeName, value: (UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)), range: NSRange(location: (str1.characters.count), length: ("\(kBaseUrl)" as String).characters.count))
        
        
        attributedString.addAttribute(NSFontAttributeName, value: (UIFont.init(name: "Helvetica-Bold", size: 28))!, range: NSRange(location:(str1.characters.count),length:("\(kBaseUrl)" as String).characters.count))
        
        
        
        attributedString.addAttribute(NSForegroundColorAttributeName, value: (UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)), range: NSRange(location:(str2.characters.count), length: ("Click ADD." as String).characters.count))
        
        
        attributedString.addAttribute(NSFontAttributeName, value: (UIFont.init(name: "Helvetica-Bold", size: 28))!, range: NSRange(location:(str2.characters.count),length:("Click ADD." as String).characters.count))
        self.CodeLabel.attributedText = attributedString
        self.view.isUserInteractionEnabled = true
        codeview.layer.borderWidth = 4.0
        codeview.layer.borderColor = UIColor.white.cgColor
        activationCode.text = code
        clickbtn.layer.cornerRadius = 4.0
        clickbtn.backgroundColor = UIColor.init(red: 179/255, green: 22/255, blue: 28/255, alpha: 1)
        timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.accountstatus), userInfo: nil, repeats: true)
    }
    
    func accountstatus()
    {
        let url = kAccountInfoUrl
        let alertview = UIAlertController(title: "Activation Status", message: "Your Device Activation is Pending" , preferredStyle: .alert)
        let  parameters = [ "getAccountInfo": ["deviceId": deviceId!, "uuid": uuid!]]
        ApiManager.sharedManager.postDataWithJson(url: url, parameters: parameters as [String : [String : AnyObject]]) {(responseDict , error,isDone) in
            if error == nil
            {
                let post = responseDict
                let dict = post as! NSDictionary
                self.datadict = dict
                if (dict.value(forKey: "uuid_exist") as! Bool) == false
                {
                    if self.isAction
                    {
                        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                        alertview.addAction(defaultAction)
                        self.present(alertview, animated: true, completion: nil)
                        self.isAction = false
                    }
                }
                else if (dict["user_status"] != nil)
                {
                    if (((dict["user_status"]!) as AnyObject).contains("active"))
                    {
                        self.userId = dict["_id"] as! String
                        
                        gUserID = dict["_id"] as! String
                        gCustomerId = dict["customer_id"] as! String
                        gUserName = dict["full_name"] as! String
                        gEmailId = dict["email"] as! String
                        UserDefaults.standard.set(gCustomerId, forKey: "customer_id")
                        UserDefaults.standard.set(gUserName, forKey: "full_name")
                        UserDefaults.standard.set(gEmailId, forKey: "email")
                        self.setUserId()
                        self.timer.invalidate()
                        self.gotoRoot()
                        
                    }
                }
            }
            else
            {
                // create erro log
            }
        }
    }
    
    //setVenderId
    func setUserId() {
        let keychain = Keychain(service: userIdentifier)
        do {
            try keychain.set(gUserID, key: userIdentifier)
            
        }
        catch _ {
           
        }
    }
    
    
    // getVenderId
    @IBAction func continuebtn(_ sender: AnyObject)
    {
        self.isAction = true
        accountstatus()
    }
    
    func gotoRoot()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.gotoMenu(userid: userId,deviceid: deviceId,uuid: self.uuid)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

