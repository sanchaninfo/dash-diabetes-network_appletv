/***
 **Module Name: CollectionContentCell
 **File Name :   CollectionContentCell.swift
 **Project :   theprogrampictures.com
 **Copyright(c) : Program Pictures.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : Asset content of table view in landing page.
 */

import UIKit
import SystemConfiguration
import Kingfisher


class CollectionContentCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet weak var menuCollectionView: UICollectionView!
    var menuCollectionList = NSMutableArray()
    var UserId,DeviceId,uuid : String!
    var isMyList = Bool()
    var CarousalDict = [[String:Any]]()
    var isPhotos = Bool()
    var isAlleyez = Bool()
  
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.menuCollectionList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContentCell", for: indexPath)
        let Path = menuCollectionList[indexPath.row] as! NSDictionary
        if Path[kMetadata] != nil
        {
            let metaDataPath = Path[kMetadata] as! NSDictionary
            if metaDataPath[kIsmenu] != nil
            {
                if metaDataPath[kIsmenu] as! Bool == true
                {
                    (cell.viewWithTag(11) as! UIImageView).image = metaDataPath[kMovieart] as? UIImage
                }
            }
            else
            {
        
                let img = (cell.viewWithTag(11) as! UIImageView)
                img.kf.indicatorType = .activity
                
                if (metaDataPath.object(forKey: kMovieart) is NSNull)
                {
                    (cell.viewWithTag(11) as! UIImageView).image = UIImage(imageLiteralResourceName:"RevoltFlat")
                    
                }
                else
                {
                    if metaDataPath[kMovieart] != nil
                    {
                        if (metaDataPath[kMovieart] as! String == "")
                        {
                            // (cell.viewWithTag(11) as! UIImageView).kf.setImage(with: URL(string: metaDataPath[kMovieart] as! String))
                            (cell.viewWithTag(11) as! UIImageView).image = UIImage(imageLiteralResourceName:"RevoltFlat")
                        }
                        else
                        {
                            (cell.viewWithTag(11) as! UIImageView).kf.setImage(with: URL(string: metaDataPath[kMovieart] as! String))
                        }
                    }
                    else
                        
                    {
                        (cell.viewWithTag(11) as! UIImageView).image = UIImage(imageLiteralResourceName:"RevoltFlat")
                    }
                    
                }
            }

        }
        
        else
        {
            let img = (cell.viewWithTag(11) as! UIImageView)
            img.kf.indicatorType = .activity
            if isPhotos == true
            {
                 (cell.viewWithTag(11) as! UIImageView).kf.setImage(with: URL(string: (kPhotoBaseUrl + (Path["thumb"] as! String))))
            }
            else
            {
              //  (cell.viewWithTag(11) as! UIImageView).kf.setImage(with: URL(string: Path["posterURL"] as! String))
                (cell.viewWithTag(11) as! UIImageView).image = UIImage(imageLiteralResourceName: "movie_thumb")
            }
        }
        collectionView.isScrollEnabled = true
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator)
    {
        let tableview:UITableView = self.superview?.superview as! UITableView
        //Previous Index Focus
        if let previousIndexPath = context.previouslyFocusedIndexPath,
            let cell = collectionView.cellForItem(at: previousIndexPath)
        {
            cell.transform = .identity
        }
        
        //Next Index Focus
        if let indexPath = context.nextFocusedIndexPath,
            let cell = collectionView.cellForItem(at: indexPath)
        {
            collectionView.scrollToItem(at: indexPath, at: [.centeredHorizontally, .centeredVertically], animated: true)
            (cell.viewWithTag(11) as! UIImageView).adjustsImageWhenAncestorFocused = true
            let viewcontroller = tableview.dataSource as! MainViewController
            if (menuCollectionList.count > indexPath.row)
            {
            let Path = menuCollectionList[indexPath.row] as! NSDictionary
            if Path[kMetadata] != nil
            {
                let image = Path[kMetadata] as! NSDictionary
                if image[kCarouselId] as! String == "Menu"
                {
                    if indexPath.row == 0
                    {
                        viewcontroller.MainImage.kf.indicatorType = .activity
                        viewcontroller.MainImage.image = UIImage(imageLiteralResourceName: "Search_shelf")
                        viewcontroller.staticLbl.text = "Search"
                        viewcontroller.lbl2.text = "Search for Dash Diabetes Network ...."
                    }
                    if indexPath.row == 1
                    {
                        viewcontroller.MainImage.kf.indicatorType = .activity
                        viewcontroller.MainImage.image = UIImage(imageLiteralResourceName: "categories_shelf")
                        viewcontroller.staticLbl.text = "Categories"
                        viewcontroller.lbl2.text = ""
                        
                    }
                    if indexPath.row == 2
                    {
                        viewcontroller.MainImage.kf.indicatorType = .activity
                        viewcontroller.MainImage.image = UIImage(imageLiteralResourceName: "settings_Shelf")
                        viewcontroller.staticLbl.text = "Settings"
                        viewcontroller.lbl2.text = "Your account information"
 
                    }
                    viewcontroller.AssestName.text = ""
                    viewcontroller.ReleaseDate.text = ""
                    viewcontroller.TimeLbl.text = ""
                    viewcontroller.Description.text = ""
                    (viewcontroller.view.viewWithTag(1))?.isHidden = true
                }
                else
                {
                    var tmpValue = String()
                    viewcontroller.staticLbl.text = ""
                    viewcontroller.lbl2.text = ""
                    viewcontroller.MainImage.kf.indicatorType = .activity
                    viewcontroller.MainImage.kf.setImage(with: URL(string: isnil(json: image, key: "main_carousel_image_url")))
                    let name = (isnil(json: Path, key: "name")).capitalized
                    let maxLength = 40
                    if name.characters.count > maxLength {
                        let range =  name.rangeOfComposedCharacterSequences(for: name.startIndex..<name.index(name.startIndex, offsetBy: maxLength))
                        tmpValue = name.substring(with: range).appending("...")
                    }
                    else
                    {
                        tmpValue = name
                    }
                    viewcontroller.likeLbl.text = String(1000)
                    viewcontroller.dislikeLbl.text = String(0)
                    viewcontroller.AssestName.text = tmpValue
                    let labelText = tmpValue
                    let lbl = viewcontroller.AssestName
                    lbl?.frame = CGRect(x: (lbl?.frame.origin.x)!, y: (lbl?.frame.origin.y)!, width: (labelText.widthWithConstrainedWidth(height: 60, font: (lbl?.font)!)), height: (lbl?.frame.size.height)!)
                    
                    let str1 = (isnil(json: image, key: "release_date")).components(separatedBy: "-")
                    viewcontroller.ReleaseDate.text = str1[0]
                    
                   //viewcontroller.TimeLbl.text =  stringFromTimeInterval(interval: Double(isnil(json: Path, key: "file_duration"))!)
                    let time = isnil(json:Path,key:"file_duration")
                    let time1 = Double(time)
                    if time1 != nil
                    {
                        viewcontroller.TimeLbl.text =  stringFromTimeInterval(interval: time1!)
                    }
                    else
                    {
                        viewcontroller.TimeLbl.text = "0 M"
                    }
                   
                    let DescriptionText = isnil(json: Path, key: "description")
                    let destxt = DescriptionText.replacingOccurrences(of: "&", with: "", options: .literal, range: nil)
                    let destxt1 =  destxt.replacingOccurrences(of: "amp", with: "", options: .literal, range: nil)
                    let destxt2 = destxt1.replacingOccurrences(of: ";", with: "", options: .literal, range: nil)
                    
                    viewcontroller.Description.text = destxt2
                    if (DescriptionText.contains("n/a"))
                    {
                        viewcontroller.Description.text = ""
                    }
                    let desLbl = viewcontroller.Description
                    desLbl?.frame = CGRect(x: (desLbl?.frame.origin.x)!, y: (desLbl?.frame.origin.y)!, width: (desLbl?.frame.size.width)!, height: (DescriptionText.widthWithConstrainedWidth(height: 60, font: (desLbl?.font)!)))
                    (viewcontroller.view.viewWithTag(1))?.isHidden = false
                }

            }
            else
            {
                if isPhotos == true
                {
                    viewcontroller.staticLbl.text = ""
                    viewcontroller.lbl2.text = ""
                    viewcontroller.MainImage.kf.indicatorType = .activity
                    viewcontroller.MainImage.kf.setImage(with: URL(string: (kPhotoBaseUrl + (Path["large"] as! String))))
                    viewcontroller.ReleaseDate.text = ""
//                    let carousel = Path["carousels"] as! NSArray
//                    let data = carousel.firstObject as! NSDictionary
                    viewcontroller.AssestName.text = (Path["title"] as! String)
                    viewcontroller.Description.text = ""
                    viewcontroller.TimeLbl.text = ""
                }
                else
                {
                    viewcontroller.staticLbl.text = ""
                    viewcontroller.lbl2.text = ""
                //    viewcontroller.MainImage.kf.indicatorType = .activity
                //    viewcontroller.MainImage.kf.setImage(with: URL(string: "http://s3.amazonaws.com/peafowl/aeom/cover.jpg"))
                    viewcontroller.MainImage.image = UIImage(imageLiteralResourceName: "movie_Poster")
                    viewcontroller.ReleaseDate.text = ""
                    let carousel = Path["carousels"] as! NSArray
                    let data = carousel.firstObject as! NSDictionary
                    viewcontroller.AssestName.text = data["carousel"] as? String
                    viewcontroller.Description.text = ""
                    viewcontroller.TimeLbl.text = Path["duration"] as? String
                }
                (viewcontroller.view.viewWithTag(1))?.isHidden = false
            }
        }
     }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let Path = menuCollectionList[indexPath.row] as! NSDictionary
       
        if Path[kMetadata] != nil
        {
            let MetaDict = Path[kMetadata] as! NSDictionary
            if ((MetaDict[kCarouselId] as! String) == "Menu")
            {
                if indexPath.row == 0
                {
                    let tableview:UITableView = self.superview?.superview as! UITableView
                    let viewcontroller = tableview.dataSource as! MainViewController
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    guard let searchResultsController = storyboard.instantiateViewController(withIdentifier: "Search") as? SearchListViewController
                        else {
                            fatalError("Unable to instatiate a SearchResultsViewController from the storyboard.")
                    }
//                    let searchController = UISearchController(searchResultsController: searchResultsController)
//                    searchController.searchResultsUpdater = searchResultsController
//                    searchController.view.backgroundColor = UIColor.init(red: 13/255, green: 13/255, blue: 13/255, alpha: 1)
//                    searchController.searchBar.setScopeBarButtonTitleTextAttributes([NSForegroundColorAttributeName:UIColor.white], for: .normal)
//                    searchController.searchBar.keyboardAppearance = UIKeyboardAppearance.dark
//                    searchController.searchBar.placeholder = NSLocalizedString("Enter keyword (e.g. The Secret to Ballin)", comment: "")
//                    let searchContainer = UISearchContainerViewController(searchController: searchController)
//                    searchContainer.title = NSLocalizedString("Search", comment: "")
                   
//                    for dict in viewcontroller.searchCollectionList
//                    {
//                        let metaDict = dict[kMetadata] as! NSDictionary
//                        let contains = dict["contains"] as! NSArray
//                        if (metaDict[kCarouselId] as! String) == "All Eyez On Me"
//                        {
//                            for i in 0 ..< (contains.count - 1)
//                            {
//                                searchResultsController.menuCollectionList.add(contains[i] as! [String:Any])
//                            }
//                        }
//                    }
                  
                    searchResultsController.searchCollectionList = viewcontroller.searchCollectionList as! NSMutableArray
                    searchResultsController.userId = viewcontroller.userId
                    searchResultsController.deviceId = viewcontroller.deviceId
                    searchResultsController.uuid = viewcontroller.uuid
                    searchResultsController.searchDelegate = viewcontroller
                   //viewcontroller.navigationController!.pushViewController(searchContainer, animated: true)
                    viewcontroller.navigationController!.pushViewController(searchResultsController, animated: true)
                    
                }
                if indexPath.row == 1
                {
                    gotoCategories()
                }
                if indexPath.row == 2
                {
                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                    let SettingsPage = storyBoard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
                    let tableview:UITableView = self.superview?.superview as! UITableView
                    let viewcontroller = tableview.dataSource as! MainViewController
                    SettingsPage.deviceId = viewcontroller.deviceId
                    SettingsPage.uuid = viewcontroller.uuid
                    viewcontroller.navigationController?.pushViewController(SettingsPage, animated: true)

                }
            }
        
            else
            {
//                if UserId != ""
//                {
//                    getaccountInfo(id:Path["id"] as! String,userid: UserId/*,tvshow: (Path["tv_show"] as! Bool)*/)
//
//                }
//                else
//                {
                self.getAssetData(withUrl: kAssestDataUrl, id: Path["id"] as! String, userid: self.UserId, subscription_status: "false",carousal_id: MetaDict["carousel_id"] as! String)

              //  }
                
            }

        }
        else
        {
            if isPhotos == true
            {
             
                let tableview:UITableView = self.superview?.superview as! UITableView
                let viewcontroller = tableview.dataSource as! MainViewController
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let PhotoPage = storyBoard.instantiateViewController(withIdentifier: "Photos") as! PhotosViewController
                PhotoPage.PhotoDict = viewcontroller.PhotoDict
                PhotoPage.ImageIndexpath = indexPath.row
                viewcontroller.navigationController?.pushViewController(PhotoPage, animated: true)
            }
            else
            {
                let tableview:UITableView = self.superview?.superview as! UITableView
                let viewcontroller = tableview.dataSource as! MainViewController
                let storyBoard = UIStoryboard(name: kBoardname, bundle: nil)
                let StorePage = storyBoard.instantiateViewController(withIdentifier: "Store") as! StoreViewController
                StorePage.prodData = Path
                StorePage.userId = UserId
                StorePage.uuid = uuid
                StorePage.deviceID = DeviceId
                viewcontroller.navigationController?.pushViewController(StorePage, animated: true)
            }
        }
    }
    
    func carousalSelected(carousalName:String,carousalDetailDict:[[String:Any]],userid:String,deviceid:String,uuid:String)
    {
        self.menuCollectionList.removeAllObjects()
        self.UserId = userid
        self.DeviceId = deviceid
        self.CarousalDict = carousalDetailDict
        self.uuid = uuid
        for dict in carousalDetailDict
        {
            if carousalName == "Menu"
            {
                self.menuCollectionList.add(dict)
            }
            else
            {
                if carousalName == "My List"
                {
                    if dict[kData] != nil
                    {
                        let dicton = dict[kData] as! NSDictionary
                        self.menuCollectionList.add(dicton)
                        self.isMyList = true
                    }
                }
                else if carousalName == "Recently Watched"
                {
                    if dict[kData] != nil
                    {
                        let dicton = dict[kData] as! NSDictionary
                        self.menuCollectionList.add(dicton)
                    }
                    else
                    {
                        self.menuCollectionList.add(dict)
                    }
                }
                else if carousalName == "Store"
                {
                    isPhotos = false
                    self.menuCollectionList.add(dict)
                    //  self.menuCollectionList.add(CarousalDict)
                    
                }
                else if carousalName == "Photos"
                {
                    isPhotos = true
                    self.menuCollectionList.add(dict)
                }
                else
                {
                    if kMetadata != ""
                    {
                        let metaDict = dict[kMetadata] as! NSDictionary
                        if (metaDict[kCarouselId] as! String) == carousalName
                        {
                            self.menuCollectionList.add(dict)
                        }
                        
                    }
                    else
                    {
                        let metaDict = dict[kMetadata] as! NSDictionary
                        if (metaDict[kCarouselId] as! String) == carousalName
                        {
                            self.menuCollectionList.add(dict)
                        }
                        
                    }
                    
                }
            }
        }
        DispatchQueue.main.async {
            self.menuCollectionView.isHidden = false
            //   activityView.removeFromSuperview()
            // self.menuCollectionView.isUserInteractionEnabled = true
            self.menuCollectionView.reloadData()
            
        }
    }
    
    // Service Call for getAssetData
    func getAssetData(withUrl:String,id:String,userid:String,/*tvshow:Bool,*/subscription_status:String,carousal_id:String)
    {
        //        if accountstatus == true
        //        {
        var parameters =  [String:[String:AnyObject]]()
        let tableview:UITableView = self.superview?.superview as! UITableView
        let viewcontroller = tableview.dataSource as! MainViewController
        let appVersionString: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        let osVersion = UIDevice.current.systemVersion
        let className = NSStringFromClass(self.classForCoder)
        var user_ID = String()
        if userid == ""
        {
            user_ID = "guest_id"
        }
        else
        {
            user_ID = userid
        }
        parameters = ["getAssestData":["videoId":id as AnyObject,"userId":user_ID as AnyObject]]
 
        ApiManager.sharedManager.postDataWithJson(url: withUrl, parameters: parameters){(responseDict , error,isDone) in
            if error == nil
            {
                let JSON = responseDict
                let storyBoard = UIStoryboard(name: kBoardname, bundle: nil)
                let tableview:UITableView = self.superview?.superview as! UITableView
                let viewcontroller = tableview.dataSource as! MainViewController

                    let DetailPage = storyBoard.instantiateViewController(withIdentifier: "DetailPage") as! DetailPageViewController
                    DetailPage.TvshowPath = JSON as! NSDictionary
                    DetailPage.userid = self.UserId
                    DetailPage.deviceId = self.DeviceId
                    DetailPage.uuid = self.uuid
                    DetailPage.delegate = viewcontroller
                    DetailPage.isMain = true
                    DetailPage.isMyList = self.isMyList
                    DetailPage.fromSearch = false
                    viewcontroller.navigationController?.pushViewController(DetailPage, animated: true)
         

            }
                
            else
            {
                let listError = (error?.localizedDescription)! as String
                UserDefaults.standard.set(listError, forKey: "locerror")
                UserDefaults.standard.synchronize()

                parameters =  ["insertLog":["source":("\(withUrl) - \(className) - \(#function)") as AnyObject,"error":(listError) as AnyObject,"code":parameters as AnyObject,"details":("userId:\(userid) - AppVersion:\(appVersionString) - OSVersion:\(osVersion)") as AnyObject,"device":"AppleTV" as AnyObject]]
                ApiManager.sharedManager.postDataWithJson(url: kLogUrl, parameters: parameters)
                {(responseDict,error,isDone)in
                    if error == nil
                    {
                        _ = responseDict
                    }
                }
            }
        }
    }
    
    func getaccountInfo(id:String,userid:String/*tvshow:Bool*/)
    {
        var parameters =  [String:[String:AnyObject]]()
        parameters = ["getAccountInfo":["deviceId":DeviceId as AnyObject,"uuid":uuid as AnyObject]]
        
        ApiManager.sharedManager.postDataWithJson(url: kAccountInfoUrl, parameters: parameters){
            (responseDict,error,isDone) in
            if error == nil
            {
                let Json = responseDict
                let dict = Json as! NSDictionary
                let accountresponse = (dict.value(forKey: "uuid_exist") as! Bool)
              
                if accountresponse
                {
                    
                  //  self.getAssetData(withUrl:kAssestDataUrl,id: id,userid: userid,/*tvshow:tvshow,*/subscription_status:(dict.value(forKey: "subscription_status") as! String))
                    self.getAssetData(withUrl: kAssestDataUrl, id: id, userid: userid, subscription_status: "false", carousal_id: "All Eyez On Me")
                }
                else
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.gotoCode()
                }
            }
        }
        
    }
    
    // Categories Controller
    func gotoCategories()
    {
        let storyBoard = UIStoryboard(name: kBoardname, bundle: nil)
        let CategoriesPage = storyBoard.instantiateViewController(withIdentifier: "Categories") as! CategoriesViewController
        let tableview:UITableView = self.superview?.superview as! UITableView
        let viewcontroller = tableview.dataSource as! MainViewController
        CategoriesPage.CategoryCarousalName = viewcontroller.CategoryCarousalName
        CategoriesPage.CarousalData = viewcontroller.CarousalData
        CategoriesPage.UserId = viewcontroller.userId
        CategoriesPage.uuid = viewcontroller.uuid
        CategoriesPage.deviceId = viewcontroller.deviceId
        CategoriesPage.MyListthumb = viewcontroller.myList
        CategoriesPage.PhotoDict = viewcontroller.PhotoDict
        CategoriesPage.storeProdData = viewcontroller.storeProdData
     //   CategoriesPage.isfromMyList = self.isMyList
        CategoriesPage.categoryDelegate = viewcontroller
        viewcontroller.navigationController?.pushViewController(CategoriesPage, animated: true)
    }
    
}
