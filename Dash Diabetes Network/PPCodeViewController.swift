/***
 **Module Name:  PPCode View Controller
 **File Name :   PPCode View Controller.swift
 **Project :   theprogrampictures.com
 **Copyright(c) : Program Pictures.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : Activation Code details for pairing the device.
 */

import UIKit
import KeychainAccess

class PPCodeViewController: UIViewController {

   
    @IBOutlet weak var gotolbl: UILabel!
    @IBOutlet var CodeView: UIView!
    var code,deviceId,uuid,userId,status:String!
    var datadict = NSDictionary()
    var timer = Timer()
    var userIdentifier = "userId"
    var existingUserId:String!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleMenuPress))
        tapGesture.allowedPressTypes = [NSNumber(value: UIPressType.menu.rawValue)]
        self.view.addGestureRecognizer(tapGesture)
        gotolbl.text = kBaseUrl + "activate"
        var xVal2 = CGFloat()
        xVal2 = 0.0
        CodeView.isUserInteractionEnabled = false
       // gotoLbl.text = "damedashstudios.com/activate"
        let str = code
        let arr = Array(str!.characters)
        for i in 0..<(str!.characters.count)
        {
            let btn = UIButton(type: .custom)
            btn.frame = CGRect(x: xVal2, y: 0, width: 65, height: 85)
            btn.setTitle("\(arr[i])", for: .normal)
            btn.setTitleColor(UIColor.white, for: .normal)
            btn.titleLabel?.font = UIFont.init(name: "OpenSans", size: 34)
            btn.backgroundColor = UIColor.init(red: 98/255, green: 98/255, blue: 98/255, alpha: 1.0)
            btn.layer.cornerRadius = 5.0
            CodeView.addSubview(btn)
            xVal2 = xVal2+btn.frame.size.width+12
        }
         starttimer()
        // timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.accountstatus), userInfo: nil, repeats: true)
    }
    
    func starttimer()
    {
        timer =  Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.accountstatus), userInfo: nil, repeats: true)
    }
    
    func stoptimer()
    {
        timer.invalidate()
    }

    
    func accountstatus()
    {
        let url = kAccountInfoUrl
        let alertview = UIAlertController(title: "Activation Status", message: "Your Device Activation is Pending" , preferredStyle: .alert)
        let  parameters = [ "getAccountInfo": ["deviceId": deviceId!, "uuid": uuid!]]
  
        ApiManager.sharedManager.postDataWithJson(url: url, parameters: parameters as [String : [String : AnyObject]]) {(responseDict , error,isDone) in
            if error == nil
            {
                let post = responseDict
           
                let dict = post as! NSDictionary
       
                self.datadict = dict
                //                if (dict.value(forKey: "uuid_exist") as! Bool) == false
                //                {
                //                    if self.isAction
                //                    {
                //                        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                //                        alertview.addAction(defaultAction)
                //                        self.present(alertview, animated: true, completion: nil)
                //                        self.isAction = false
                //                    }
                //                }
                if (dict["user_status"] != nil)
                {
                   
                    if (((dict["user_status"]!) as AnyObject).contains("active"))
                    {
                        
                        self.userId = dict["_id"] as! String
                        
                        gUserID = dict["_id"] as! String
                     //   User_ID = dict["_id"] as! String
                        gCustomerId = dict["customer_id"] as! String
                        gUserName = dict["full_name"] as! String
                        gEmailId = dict["email"] as! String
                        UserDefaults.standard.set(gCustomerId, forKey: "customer_id")
                        UserDefaults.standard.set(gUserName, forKey: "full_name")
                        UserDefaults.standard.set(gEmailId, forKey: "email")
                        self.setUserId()
                        self.timer.invalidate()
                        self.gotoRoot()
                        
                    }
                }
            }
            else
            {
                print("json error")
                //                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                //                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                //                    UIAlertAction in
                //                })
                //                alertview.addAction(defaultAction)
                //                self.navigationController?.pushViewController(alertview, animated: true)
            }
        }
    }
    //setVenderId
    func setUserId() {
        let keychain = Keychain(service: userIdentifier)
        do {
            try keychain.set(gUserID, key: userIdentifier)
            
        }
        catch _ {
            
        }
    }
    func handleMenuPress()
    {
        stoptimer()
        let alertview = UIAlertController(title: "Do you want to Exit Application", message: "" , preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
              exit(0)
            })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            self.starttimer()
            self.dismiss(animated: true, completion: nil)
        }
        alertview.addAction(defaultAction)
        alertview.addAction(cancelAction)

        self.present(alertview, animated: false, completion: nil)
 
    }
    func gotoRoot()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.gotoMenu(userid: self.userId,deviceid: self.deviceId,uuid:self.uuid)
    }
    
    @IBAction func cancelBtn(_ sender: Any) {
        self.timer.invalidate()
        let alertview = UIAlertController(title: "Do you want to Exit Application", message: "" , preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
            UIAlertAction in
            exit(0)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            //  self.navigationController?.popViewController(animated: false)
            self.starttimer()
            self.dismiss(animated: true, completion: nil)
        }
        alertview.addAction(defaultAction)
        alertview.addAction(cancelAction)
        //  self.navigationController?.pushViewController(alertview, animated: true)
        self.present(alertview, animated: false, completion: nil)
   
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
