

/***
 **Module Name:  PlayerEndViewController.
 **File Name :  PlayerEndViewController.swift
 **Project :   dev.damedashstudios.com
 **Copyright(c) : Dam Dash Studious.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : Next asset Details.
 */

import UIKit

protocol playerEndDelegate:class{
    func playEnd(userId: String, videoId: String, deviceId: String, MyList: Bool,videoURL:String)
}

class PlayerEndViewController: UIViewController {
    
    var nextCollectionList = NSMutableArray()
    var nextrowID = Int()
    var userID = String()
    var deviceID = String()
    var nextData = NSDictionary()
    var videoDict = NSDictionary()
    var isfromMyList = Bool()

    @IBOutlet weak var bgImg: UIImageView!
  
    @IBOutlet weak var titleLbl: UILabel!
   
    @IBOutlet weak var automated: UILabel!
    @IBOutlet weak var desLbl: UILabel!
    @IBOutlet weak var thumbImg: UIImageView!

    @IBOutlet weak var duration: UILabel!
    var myTimer = Timer()
    var i = Int()
    var playEnddelegate:playerEndDelegate?
    var videoUrl = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        i = 5
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleMenuPress))
        tapGesture.allowedPressTypes = [NSNumber(value: UIPressType.menu.rawValue)]
        self.view.addGestureRecognizer(tapGesture)
        let metaData = nextData[kMetadata] as! NSDictionary
        let bgImage = isnil(json: metaData, key: "main_carousel_image_url")
        let thumbImage = isnil(json: metaData, key: "movie_art")
        bgImg.kf.setImage(with: URL(string: bgImage))
        thumbImg.kf.setImage(with: URL(string: thumbImage))
        titleLbl.text = isnil(json: nextData, key: "name")
        desLbl.text = isnil(json: nextData, key: "description")
        let time = isnil(json: nextData, key: "file_duration")
        let time1 = Double(time)
        if time1 != nil
        {
             duration.text =  stringFromTimeInterval(interval: time1!)
        }
        else
        {
             duration.text = "0 M"
        }
       
    }

    func handleMenuPress()
    {
        for viewcontroller in (self.navigationController?.viewControllers)!
        {
            if viewcontroller.isKind(of: DetailPageViewController.self)
            {
                 let  _ =  self.navigationController?.popToViewController(viewcontroller, animated: false)
            //    let _ = self.navigationController?.pushViewController(viewcontroller, animated: false)
            }
        }
    }
    func getData(getnextData:NSDictionary)
    {
        self.nextData = getnextData
        RecentlyWatched(withurl:kRecentlyWatchedUrl)
        myTimer = Timer.scheduledTimer(timeInterval: 1.1, target: self, selector: #selector(self.getplay), userInfo: nil, repeats: true)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        i = 5
    }
    func getplay()
    {
        automated.text = "\(String(i)) SEC"
        i = i-1
        if i == 0
        {
          myTimer.invalidate()
          print("player is ready")
          playvideo()
        }
        
    }
    
    @IBAction func playButton(_ sender: Any) {
        myTimer.invalidate()
        playvideo()
    }
    
    func playvideo()
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let PlayerVC = storyBoard.instantiateViewController(withIdentifier: "playerLayer") as!PlayerLayerViewController
      
            PlayerVC.isResume = false
            if nextData["url_m3u8"] != nil && nextData["url_m3u8"] as! String != ""
            {
               videoUrl = nextData["url_m3u8"] as! String
            }
            else
            {
               videoUrl = nextData["url"] as! String
            }
            PlayerVC.mainVideoID = nextData["id"] as! String
            PlayerVC.userID = userID
            PlayerVC.deviceID = deviceID
            PlayerVC.videoID = (nextData["id"] as! String)
            PlayerVC.isMyList = self.isfromMyList
            PlayerVC.isdurPlay = false
            PlayerVC.isplayendcalled = false
            PlayerVC.videoUrl = videoUrl
      
            self.playEnddelegate?.playEnd(userId: self.userID, videoId: nextData["id"] as! String, deviceId: self.deviceID, MyList: self.isfromMyList,videoURL: self.videoUrl)
        let controllers = self.navigationController!.viewControllers
        
        for controller in controllers
        {
            
            if controller.isKind(of: PlayerLayerViewController.self)
            {
                self.navigationController?.popViewController(animated: true)
            }
        }
       //     self.navigationController?.pushViewController(PlayerVC, animated: false)
         //  let _ = self.navigationController?.popViewController(animated: true)
        
    }
    func RecentlyWatched(withurl:String)
    {
        let parameters = ["createRecentlyWatched": ["videoId":nextData["id"] as! String,"userId":userID as AnyObject]]
        print(parameters)
        ApiManager.sharedManager.postDataWithJson(url: withurl, parameters: parameters as [String : [String : AnyObject]]){(responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict
            //    let dict = JSON as! NSDictionary
           //     let dicton = dict["recentlyWatched"] as! NSDictionary
//                if (dicton["seekTime"] as! Float64) > 0
//                {
//                  
//                }
            }
            else
            {
                print("json error")

            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
