
/***
 **Module Name:  PlayerLayerView Controller.
 **File Name :  PlayerLayerView Controller.swift
 **Project :   dev.damedashstudios.com
 **Copyright(c) : Dam Dash Studious.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : Player.
 */
import UIKit
import AVKit

protocol playerdelegate:class {
    func getassetdata(withUrl:String,id:String,userid:String)
}

class PlayerLayerViewController: UIViewController,playerEndDelegate {
    
    var playerLayer:AVPlayerLayer!
    
    
    var playerItem:AVPlayerItem!
    
    var videoUrl,userID,videoID,mainVideoID,deviceID : String!
    var seektime = Float64()
    var updatetime = Float64()
    var isResume = Bool()
    var detdelegate:playerdelegate?
    var resumeTime = Float64()
    var getnextAsset = Bool()
    var UpdateTimer = Timer()
    var isplayEnd = Bool()
    var nextAsset = NSDictionary()
    var isMyList = Bool()
    var nexttimer = Timer()
    var player:AVPlayer!
    var isMenuPressed = Bool()
    var timeObserver:Any?
    var notifyObserver:Any?
    var isdurPlay = Bool()
    var avplayerController = AVPlayerViewController()
    var issearch = Bool()
    var isplayendcalled = Bool()
    var currentDuration = 0
    var previousDuration = 0
    var notPlayCheckCnt = 0
    var duration = 0
    var playstatusTimer = Timer()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleMenuPress))
        tapGesture.allowedPressTypes = [NSNumber(value: UIPressType.menu.rawValue)]
        self.view.addGestureRecognizer(tapGesture)
        avplayerController.view.frame = self.view.frame
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isdurPlay = false
        self.isplayendcalled = false
        self.getnextAsset = false
        self.isMenuPressed = false
//        playerItem = AVPlayerItem(url: NSURL(string: videoUrl)! as URL)
//        player = AVPlayer(playerItem:playerItem)
//        player.replaceCurrentItem(with: playerItem)
//        avplayerController.player = player
//        self.view.addSubview(avplayerController.view)
//        self.addChildViewController(avplayerController)
       
        playVideo(userId: self.userID, videoId: self.videoID, deviceId: self.deviceID, MyList: self.isMyList)
        
    }
    
    func handleMenuPress()
    {
        self.isMenuPressed = true
   //     UpdateTimer.invalidate()
         avplayerController.player?.pause()
         avplayerController.player?.removeTimeObserver(timeObserver as Any)
      avplayerController.player = nil
        if !issearch
        {
            self.detdelegate?.getassetdata(withUrl:kAssestDataUrl,id:self.videoID,userid:self.userID)
            
            let _ = self.navigationController?.popViewController(animated: true)
        }
            
        else
        {
            dismiss(animated: true, completion: nil)
            
        }
    }
    
    
    // playvideo Implementation
    func playVideo(userId:String,videoId:String,deviceId:String,MyList:Bool) {
        
        var player = AVPlayer()
        let asset = AVAsset(url: URL(string:videoUrl)!)
        let item=AVPlayerItem(asset: asset)
      
        player = AVPlayer(playerItem: item)
        player.replaceCurrentItem(with: item)
        avplayerController.player = player
        avplayerController.view.frame = CGRect(x: 0, y: 0, width: 1920, height: 1080)
        self.view.addSubview(avplayerController.view)
        self.addChildViewController(avplayerController)
        var updateSeekCnt = 0
        userID = userId
        videoID = videoId
        deviceID = deviceId
        isMyList = MyList
        if isResume
        {
            
            let targetTime = CMTime(seconds: resumeTime, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
            player.seek(to: targetTime, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
            player.play()
        }
        else
        {
            let targetTime = CMTime(seconds: 0.0, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
            player.seek(to: targetTime, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
            player.play()
        }
          playstatusTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.playstatus), userInfo: nil, repeats: true)
     
    
        timeObserver = (player.addPeriodicTimeObserver(forInterval: CMTimeMake(1, 1), queue: DispatchQueue.main, using:
            {_ in
                
                if player.currentItem?.status == .readyToPlay
                {
//                   // if self.duration <= 0
//                   // {
//                        self.duration = Int(CMTimeGetSeconds((self.playerItem.duration)))
//                 //   }
//                        if(updateSeekCnt >= 10)
//                    {
//                        updateSeekCnt = 0
//                       self.UpdateseekTime()
//                        
//                    }
//                    updateSeekCnt = updateSeekCnt + 1
//                    
//                    self.seektime = CMTimeGetSeconds((self.playerItem.currentTime()))
//                    self.currentDuration = Int ( CMTimeGetSeconds((self.playerItem.currentTime())))
//                    
//                    
//                    if(self.currentDuration != self.previousDuration)
//                    {
//                        self.previousDuration =  self.currentDuration
//                        self.notPlayCheckCnt = 0
//                    }
//                    else
//                    {
//                        self.notPlayCheckCnt = self.notPlayCheckCnt + 1
//                    }
//                    if ( self.notPlayCheckCnt >= 10 )
//                    {
//                        self.playerEnd()
//                    }
//                    if ((self.duration - self.currentDuration) <= 30) && (self.getnextAsset == false)
//                    {
//                        self.getnextAsset = true
//                        if self.issearch == false
//                        {
//                            self.nextAssetData()
//                            self.isMenuPressed = true
//                        }
//                    }
//                 
                /*    if  (self.currentDuration + 1) >= Int(self.duration) && self.isdurPlay == false
                    {
                        self.isdurPlay = true
//                        if self.issearch == false
//                        {
                            if self.isplayendcalled == false
                            {
                                self.isplayendcalled = true
                                self.playerEnd()
                            }
//                        }
                    }*/
                    self.seektime = CMTimeGetSeconds((player.currentItem?.currentTime())!)
                    let duration = CMTimeGetSeconds((player.currentItem?.duration)!)
                    if ((duration - self.seektime) <= 30.0) && (self.getnextAsset == false)
                    {
                        if self.issearch == false
                        {
                            self.nextAssetData()
                            self.isMenuPressed = true
                        }
                    }
                    if (self.seektime >= (duration - 1.0)) && (self.isdurPlay == false)
                    {
                        self.isdurPlay = true
                        if self.issearch == false
                        {
                            self.playerEnd()
                        }
                    }
                    
                 /*   if (stime >= (dur-2)) && (self.isdurPlay == false)
                    {
                        self.isdurPlay = true
                        if self.issearch == false
                        {
                            if self.isplayendcalled == false
                            {
                                self.isplayendcalled = true
                                self.playerEnd()
                            }
                        }
                    }*/
                }
        }))
        
        
     //   UpdateTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.nextplaycheck), userInfo: nil, repeats: true)
        
    }
    
//    func nextplaycheck()
//    {
//        if player.rate == 0
//        {
//            print("player is not playing")
//        }
//    }
    
    func playnextVideo()
    {
        let playerItem = AVPlayerItem(url: NSURL(string: videoUrl)! as URL)
        player = AVPlayer(playerItem: playerItem)
        player?.play()
    }
    
    // seektime
    func UpdateseekTime()
    {
        let parameters = ["updateSeekTime":["userId": userID as AnyObject, "videoId": (videoID) as AnyObject, "seekTime": self.seektime]]
        
        ApiManager.sharedManager.postDataWithJson(url: kUpdateseekUrl, parameters: parameters as [String : [String : AnyObject]]){(responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict as! NSDictionary
                if JSON["watchedVideo"] != nil
                {
                    if JSON["watchedVideo"] is NSDictionary
                    {
                        let dict = JSON["watchedVideo"] as! NSDictionary
                        self.updatetime = Float64((dict["seekTime"]) as! Float64)
                        UserDefaults.standard.set(self.updatetime, forKey: "seektime")
                        UserDefaults.standard.synchronize()
//                        if self.updatetime > 0
//                        {
//                            self.detdelegate?.getassetdata(withUrl:kAssestDataUrl,id:self.videoID,userid:self.userID)
//                        }
                    }
                }
            }
            else
            {
                print("json error")
                //                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                //                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                //                    UIAlertAction in
                //                  let _ = self.navigationController?.popViewController(animated: true)
                //                })
                //                alertview.addAction(defaultAction)
                //                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }
    }
    
    // play status
    func playstatus()
    {
      //  var player:AVPlayer!
        
        if avplayerController.player?.currentItem?.status == .readyToPlay
        {
            print("avplayer is ready")
            avplayerController.player?.play()
            playstatusTimer.invalidate()
            //   self.player.removeTimeObserver(self.Observer as Any)
        }
        if avplayerController.player?.currentItem?.status == .unknown
        {
            print("player is unknown")
        }
        if avplayerController.player?.currentItem?.status == .failed
        {
            //  self.childView.removeFromSuperview()
            playstatusTimer.invalidate()
            
            print("player is failed")
            print(avplayerController.player?.error)
            print(avplayerController.player?.currentItem?.error)
            let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                UIAlertAction in
                let controllers = self.navigationController!.viewControllers
                var i = 0
                for controller in controllers
                {
                    i = i + 1
                    if controller.isKind(of: PlayerLayerViewController.self)
                    {
                        let nav = self.navigationController
                        var stack = nav?.viewControllers
                        stack?.remove(at: (i - 1))
                        nav?.setViewControllers(stack!, animated: false)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            })
            alertview.addAction(defaultAction)
            self.navigationController?.present(alertview, animated: false, completion: nil)
            
        }
    }

    
    // Next Asset Data
    func nextAssetData()
    {
        print("Iam in next Asset Data")
        getnextAsset = true
        let parameters = ["getNextPlay": ["videoId": videoID, "userId": self.userID,"myList":self.isMyList,"deviceId":self.deviceID]]
        
      
        ApiManager.sharedManager.postDataWithJson(url:kNextAssetUrl ,parameters: parameters as [String : [String : AnyObject]])
        {
            (responseDict,error,isDone)in
            if error == nil
            {
                self.nextAsset = responseDict as! NSDictionary
                nextPlayData = responseDict as! NSDictionary
            }
            else
            {
                
            }
        }
        
    }
    // player end
    func playerEnd()
    {
        print("Iam in player end")
        seektime = 0.0
        self.isplayEnd = true
        UpdateseekTime()
     //   UpdateTimer.invalidate()
        avplayerController.player?.removeTimeObserver(timeObserver as Any)
         avplayerController.player = nil
        getnextAsset = false
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let playerEnd = storyBoard.instantiateViewController(withIdentifier: "playerEnd") as! PlayerEndViewController
        playerEnd.userID = userID
        playerEnd.deviceID = deviceID
        playerEnd.isfromMyList = self.isMyList
        if nextAsset.count != 0
        {
            playerEnd.playEnddelegate = self
            //  playerEnd.nextData = nextAsset
            playerEnd.getData(getnextData: nextAsset)
        //    isdurPlay = false
         //   isplayendcalled = false
            self.navigationController?.pushViewController(playerEnd, animated: true)
        }
        else
        {
            if self.issearch
            {
                 dismiss(animated: true, completion: nil)
            }
            else
            {
               let _ = self.navigationController?.popViewController(animated: true)
            }
            
        }
        
    }
    func playEnd(userId: String, videoId: String, deviceId: String, MyList: Bool,videoURL:String)
    {
        self.userID = userId
        self.videoID = videoId
        self.deviceID = deviceId
        self.isMyList = MyList
        self.videoUrl = videoURL
    }
}

