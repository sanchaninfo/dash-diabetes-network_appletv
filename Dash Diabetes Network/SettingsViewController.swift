
/***
 **Module Name:  SettingsViewController.
 **File Name : SettingsViewController.swift
 **Project :   dev.damedashstudios.com
 **Copyright(c) : Dam Dash Studious.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : setting Page.
 */

import UIKit

class SettingsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var menu = [String]()
    var deviceId,uuid : String!
    override func viewDidLoad() {
        super.viewDidLoad()
        menu.append("My Account")
        menu.append("Get help")
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        (cell.viewWithTag(11) as! UILabel).text = menu[indexPath.row]
        cell.layer.cornerRadius = 7.0
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didUpdateFocusIn context: UITableViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if let previous = context.previouslyFocusedIndexPath,
            let cell = tableView.cellForRow(at: previous)
        {
            (cell.viewWithTag(11) as! UILabel).textColor = UIColor.white
        }
        if let next = context.nextFocusedIndexPath,
            let cell = tableView.cellForRow(at: next)
        {
            (cell.viewWithTag(11) as! UILabel).textColor = UIColor.black
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0
        {
            accountstatus()
        }
    }
    
    func accountstatus()
    {
        let url = kAccountInfoUrl
        let  parameters = [ "getAccountInfo": ["deviceId": deviceId!, "uuid": uuid!]]
        ApiManager.sharedManager.postDataWithJson(url: url, parameters: parameters as [String : [String : AnyObject]]) {(responseDict , error,isDone) in
            if error == nil
            {
                let post = responseDict
                let dict = post as! NSDictionary
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let accountInfo = storyBoard.instantiateViewController(withIdentifier: "Account") as! AccountInfoViewController
                accountInfo.accountDict = dict
                
                if (dict["uuid_exist"] as! Bool) == false
                {
                    self.pairingPage()
                }
                else
                {
                    self.navigationController?.pushViewController(accountInfo, animated: true)
 
                }
            }
            else
            {
               // create a error log
            }
        }
    }
    
    func pairingPage()
    {
        var parameters = [String:[String:AnyObject]]()
        parameters = ["getActivationCode":["model":"AppleTV4Gen" as AnyObject,"manufacturer":"Apple" as AnyObject,"device_name":"AppleTV" as AnyObject, "device_id":deviceId! as AnyObject,"device_mac_address":"mac" as AnyObject,"brand_name":"Apple" as AnyObject,"host_name":"app" as AnyObject,"display_name":"apple" as AnyObject, "serial_number":"1234" as AnyObject,"version":"DameDashStudios" as AnyObject,"platform":"TVOS" as AnyObject]]
        ApiManager.sharedManager.postDataWithJson(url: kActivationCodeUrl, parameters: parameters) { (responseDict, error,isDone)in
            if error == nil
            {
                let dict = responseDict as! [String:String]
                self.uuid = dict["uuid"]
                self.deviceId = dict["device_id"]
                
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                //                                    if !((dict["pair_devce"]! as String).contains("active"))
                //                                    {
                //    let rootView = storyBoard.instantiateViewController(withIdentifier: "Code") as! CodeViewController
                let rootView = storyBoard.instantiateViewController(withIdentifier: "PPCode") as! PPCodeViewController
                rootView.uuid = self.uuid
                rootView.deviceId = self.deviceId
                rootView.code = dict["code"]
//                if self.fromSearch
//                {
//                    self.present(rootView, animated: false, completion: nil)
//                }
//                else
//                {
                    self.navigationController?.pushViewController(rootView, animated: true)
//                }
                // }
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
