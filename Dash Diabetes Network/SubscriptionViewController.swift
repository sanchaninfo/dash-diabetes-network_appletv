
/***
 **Module Name: SubscriptionViewController
 **File Name :  SubscriptionViewController.swift
 **Project :   dev.damedashstudios.com
 **Copyright(c) : Dam Dash Studious.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : Subscription expired
 */
import UIKit

class SubscriptionViewController: UIViewController {

    @IBOutlet weak var subLbl: UILabel!
    @IBOutlet weak var outView: UIView!
    @IBOutlet weak var logoImg: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        outView.layer.cornerRadius = 4.0
        outView.layer.borderWidth = 4.0
        
        
        outView.layer.borderColor = UIColor.white.cgColor
        logoImg.layer.cornerRadius = logoImg.frame.height/2
        logoImg.clipsToBounds = true
      //  outView.layer.borderColor = UIColor.init(red: 39/255, green: 39/255, blue: 39/255, alpha: 1.0).cgColor
      
        let str = "Your subscription has been expired.\nPlease upgrade subscription at \n \(kBaseUrl)"
        let attributedString = NSMutableAttributedString.init(string: str)
        self.subLbl.attributedText = attributedString
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
